﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class TankSelection : MonoBehaviour
{
    private GameObject[] tankList;
    private int selectionIndex;



    private void Start()
    {
        selectionIndex = PlayerPrefs.GetInt("TankSelected");

        tankList = new GameObject[transform.childCount];

        // Fill array with all of our models. Add prefab to gameobject per selectable tank type

        for (int i = 0; i < transform.childCount; i++)
            tankList[i] = transform.GetChild(i).gameObject;

        // We toggle off their renderer

        foreach (GameObject go in tankList)
            go.SetActive(false);

        // We toggle on selected character

        if (tankList[selectionIndex])
            tankList[selectionIndex].SetActive(true);


    }

    public void ToggleLeft()
    {
        // Toggle off current model

        tankList[selectionIndex].SetActive(false);

        selectionIndex--;
        if (selectionIndex < 0)
            selectionIndex = tankList.Length - 1;

        // Toggle on new model

        tankList[selectionIndex].SetActive(true);

    }

    public void ToggleRight()
    {
        // Toggle off current model

        tankList[selectionIndex].SetActive(false);

        selectionIndex++;
        if (selectionIndex == tankList.Length)
            selectionIndex = 0;

        // Toggle on new model

        tankList[selectionIndex].SetActive(true);

    }

    public void ConfirmButton()
    {
        // Need to track which player is selecting. 
        // If player 1, Confirm Button needs to then start selection for next player
        // If last player, then load Main scene as shown below

        PlayerPrefs.SetInt("TankSelected", selectionIndex);

        SceneManager.LoadScene("Main");
    }


}
